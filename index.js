const CaV = require('chapter-and-verse');
const books = require('./books');

/**
     * Retrieve bible book number
     *
     * @param {String} book - bible book
     * @return {Number} returns number ID
     */
module.exports = function (book) {
    let bookID = CaV(book).book.id;
    let i = 0;
    while (books[i].id != bookID) {
        i++;
    }
    if (i == 66) {
        throw new Error('Book ' + book + ' does not seem to exist.')
    }
    return i;
}