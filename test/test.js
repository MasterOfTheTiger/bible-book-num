'use strict';

var expect = require('chai').expect;
var bookNum = require('../index');

describe('#bible-book-num', function() {
    it('should return ID number of book when given short name', function() {
        var result = bookNum('Gen');
        expect(result).to.equal(0);
    });
    it('should return ID number of book when given long name', function() {
        var result = bookNum('Genesis');
        expect(result).to.equal(0);
    });
    it('should return ID number of book when given reference', function() {
        var result = bookNum('Genesis 4:2');
        expect(result).to.equal(0);
    });
});