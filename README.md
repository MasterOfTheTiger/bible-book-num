# Bible Book Number
This is a simple Node module that gives a book ID number when given a book of the Bible.

Note: It counts from zero.

## Installation
```
npm i -S bible-book-num
```

## Usage
```
const book-num = require('bible-book-num')
console.log(book-num(Genesis)) // Should return 0
```

## Credits
Copyright (c) 2018 MasterOfTheTiger. MIT License. 

books.json file from [Chapter and Verse](https://github.com/danday74/chapter-and-verse/blob/master/js/books.json) by danday74. ISC License.